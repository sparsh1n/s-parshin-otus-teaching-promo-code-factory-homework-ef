﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class SQLiteDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public SQLiteDbContext(DbContextOptions<SQLiteDbContext> options) : base(options)
        {

        }

        public SQLiteDbContext()
        {
            //Database.EnsureDeleted(); // проба поиграться с миграциями, в таком случае удалять БД не надо

            //if (Database.EnsureCreated())
            //{
            //    this.Roles.AddRange(FakeDataFactory.Roles);
            //    this.Employees.AddRange(FakeDataFactory.Employees);
            //    this.Preferences.AddRange(FakeDataFactory.Preferences);
            //    this.Customers.AddRange(FakeDataFactory.Customers);

            //    this.SaveChanges();
            //}
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlite("Filename=PromoCodeFactory.sqlite")
                .UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
            .HasKey(t => new { t.CustomerId, t.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(cp => cp.PreferenceId);


            ///modelBuilder.ApplyConfiguration(new CustomerPreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new PreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PromoCodeConfiguration());
            

            //modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            //modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            //modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            //modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        }
    }
}
