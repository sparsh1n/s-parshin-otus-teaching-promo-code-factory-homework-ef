﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {

        private readonly SQLiteDbContext _sqliteDbContext;

        public EfRepository(SQLiteDbContext sqliteDbContext)
        {
            _sqliteDbContext = sqliteDbContext;
        }

        public async Task AddAsync(T entity)
        {
            await _sqliteDbContext.Set<T>().AddAsync(entity);
            await _sqliteDbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _sqliteDbContext.Set<T>().Remove(entity);
            await _sqliteDbContext.SaveChangesAsync();

        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _sqliteDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _sqliteDbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(T entity)
        {
            await _sqliteDbContext.SaveChangesAsync();


        }
    }

}