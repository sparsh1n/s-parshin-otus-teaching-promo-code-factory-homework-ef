﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder
                .Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(100);

            builder
                .Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(100);

            builder
                .Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(100);

            //builder.HasOne(e => e.Role).WithMany(t => t.Employees).HasForeignKey(r => r.RoleId);
        }
    }
}
