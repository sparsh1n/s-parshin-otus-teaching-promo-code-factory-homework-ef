﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder
                .Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder
                .Property(r => r.Description)
                .HasMaxLength(500);
        }
    }
}
