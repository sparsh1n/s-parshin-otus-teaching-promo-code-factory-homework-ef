﻿using System.Threading.Tasks;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
            : IDbInitializer
    {
        private readonly SQLiteDbContext _sqliteDbContext;

        public EfDbInitializer(SQLiteDbContext dataContext)
        {
            _sqliteDbContext = dataContext;
        }

        public void InitializeDb()
        {
            // применяем миграции, убираем удаление базы.
            //_sqliteDbContext.Database.EnsureDeleted(); 
            //_sqliteDbContext.Database.EnsureCreated()
            if (!_sqliteDbContext.Roles.Any())
            {
                _sqliteDbContext.Roles.AddRange(FakeDataFactory.Roles);
            }
            if (!_sqliteDbContext.Roles.Any())
            {
                _sqliteDbContext.Employees.AddRange(FakeDataFactory.Employees);
            }
            if (!_sqliteDbContext.Roles.Any())
            {
                _sqliteDbContext.Preferences.AddRange(FakeDataFactory.Preferences);
            }
            if (!_sqliteDbContext.Roles.Any())
            {
                _sqliteDbContext.Customers.AddRange(FakeDataFactory.Customers);
            }
            
            _sqliteDbContext.SaveChanges();
        }
    }

}
