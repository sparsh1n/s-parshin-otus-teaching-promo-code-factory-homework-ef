﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(c =>
               new CustomerShortResponse
               {
                   Id = c.Id,
                   Email = c.Email,
                   FirstName = c.FirstName,
                   LastName = c.LastName
               }
                );

            return Ok(response);
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.CustomerPreferences.Select(cp => new PreferenceResponse()
                {
                    Id = cp.Preference.Id,
                    Name = cp.Preference.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes.Select(pc => new PromoCodeShortResponse()
                {
                    Id = pc.Id,
                    Code = pc.Code,
                    ServiceInfo = pc.ServiceInfo,
                    BeginDate = pc.BeginDate.ToShortDateString(),
                    EndDate = pc.BeginDate.ToShortDateString(),
                    PartnerName = pc.PartnerName
                }).ToList()
            };

            return Ok(response);

        }

        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {

            if ((await _customerRepository.GetAllAsync()).Any(t => t.Email == request.Email))
            {
                return BadRequest($"Customer with the same Email: {request.Email} is already exists");
            }

            var customer = new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };

            var preferences = (await _preferenceRepository.GetAllAsync()).Where(t => request.PreferenceIds.Contains(t.Id));

            customer.CustomerPreferences = preferences.Select(x => new CustomerPreference()
            {
                Customer = customer,
                Preference = x
            }).ToList();

            await _customerRepository.AddAsync(customer);

            return Ok();

        }

        /// <summary>
        /// Изменить данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:Guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound($"Customer with Id: {id} not found in DB");
            }
            else if (request.Email == customer.Email)
            {
                return BadRequest($"Customer with the same Email: {request.Email} is already exists");
            }

            var preferences = (await _preferenceRepository.GetAllAsync()).Where(t => request.PreferenceIds.Contains(t.Id));

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences.Clear();
            customer.CustomerPreferences = preferences
                .Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = x
                }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:Guid}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();

        }
    }
}