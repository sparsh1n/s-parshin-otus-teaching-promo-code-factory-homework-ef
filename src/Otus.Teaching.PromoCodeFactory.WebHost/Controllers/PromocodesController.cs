﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Employee> _employeeReposistory;

        public PromocodesController(
            IRepository<PromoCode> promoCodesRepository, IRepository<Customer> customersRepository, IRepository<Employee> employeeReposistory)
        {
            _promoCodesRepository = promoCodesRepository;
            _customersRepository = customersRepository;
            _employeeReposistory = employeeReposistory;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();

            var response = promoCodes.Select(pc =>
               new PromoCodeShortResponse
               {
                   Id = pc.Id,
                   Code = pc.Code,
                   ServiceInfo = pc.ServiceInfo,
                   BeginDate = pc.BeginDate.ToShortDateString(),
                   EndDate = pc.EndDate.ToShortDateString(),
                   PartnerName = pc.PartnerName
               }
                );

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customerPreferences = (await _customersRepository.GetAllAsync())
                .SelectMany(c => c.CustomerPreferences)
                .Where(cp => cp.Preference.Name == request.Preference).ToList();

            if (!customerPreferences.Any())
            {
                return NotFound($"Can't find Customer with Preference: {request.Preference}");
            }

            foreach (var cp in customerPreferences)
            {
                var employees = await _employeeReposistory.GetAllAsync();

                var employee = employees.FirstOrDefault(e => e.FullName == request.PartnerName);
                if (employee is null)
                {
                    employee = employees.FirstOrDefault(e => e.LastName == request.PartnerName);
                }
                if (employee is null)
                {
                    return NotFound($"Can't find Employee with name: {request.PartnerName}");
                }

                // создаем промокод
                var promoCode = new PromoCode
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMonths(1),
                    PartnerName = request.PartnerName,
                    CustomerId = cp.CustomerId,
                    Customer = cp.Customer,
                    PartnerManager = employee,
                    Preference = cp.Preference
                };

                await _promoCodesRepository.AddAsync(promoCode);

                //добавляем его к кастомеру.
                var customer = cp.Customer;
                customer.PromoCodes.Add(promoCode);
                await _customersRepository.UpdateAsync(customer);

            }

            return Ok();
        }
    }
}