﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        [Required, MaxLength(500)]
        public string ServiceInfo { get; set; }
        [Required, MaxLength(100)]
        public string PartnerName { get; set; }
        [Required, MaxLength(100)]
        public string PromoCode { get; set; }
        [Required, MaxLength(100)]
        public string Preference { get; set; }
    }
}